# Übung zur Vorlesung "Datenvisualisierung"
# SS 2020
# Aufgabe_Glyph

import vtk
import math

# Es soll ein Vektorfeld durch Pfeile dargestellt werden aehnlich
# dem Experiment, bei dem ein Feld von kleinen Kompaßnadeln
# ein Magnetisches Feld anzeigt.
# 
# Bearbeiten Sie die mit "TODO" gekennzeichneten Stellen.


# Diese Funktion liefert Ihnen das Vektorfeld:
def create_vectorfield(dim1, dim2):
	# compute positions
	ax = dim1 / 4.0
	ay = dim2 / 2.0
	bx = dim1 / 4.0 * 3.0
	by = dim2 / 2.0

	# force sources
	aforce = 100.0
	bforce = -100.0

	grid = vtk.vtkStructuredGrid()
	grid.SetDimensions(dim1, dim2, 1)

	# points
	points = vtk.vtkPoints()
	points.Allocate(0, dim1 * dim2)

	# one force vector for each point
	vectors = vtk.vtkDoubleArray()
	vectors.SetNumberOfComponents(3)
	vectors.SetNumberOfTuples(dim1 * dim2)

	# for each point the incoming force
	scalars = vtk.vtkDoubleArray()
	scalars.SetNumberOfComponents(1)
	scalars.SetNumberOfTuples(dim1 * dim2)

	for i in range(dim1):
		
		for j in range(dim2):
			
			offset = i * dim2 + j
			da = math.pow(i - ax, 2.0) + math.pow(j - ay, 2.0)
			db = math.pow(i - bx, 2.0) + math.pow(j - by, 2.0)

			# force 
			v1 = 0.0
			v2 = 0.0
			if da != 0.0:
				v1 = aforce / (4.0 * math.pi * da)
			else:
				v1 = math.inf

			if db != 0.0:
				v2 = bforce / (4.0 * math.pi * db)
			else:
				v2 = math.inf

			# direction
			if da != 0.0:
				x1 = (i - ax) / math.sqrt(da)
				y1 = (j - ay) / math.sqrt(da)
			else:
				x1 = 0
				y1 = 0

			if db != 0.0:
				x2 = (i - bx) / math.sqrt(db)
				y2 = (j - by) / math.sqrt(db)
			else:
				x2 = 0
				y2 = 0

			x = v1*x1 + v2*x2
			y = v1*y1 + v2*y2
			s = math.fabs(v1 + v2)

			points.InsertPoint(offset, i, j, 0)
			vectors.InsertTuple3(offset, x, y, 0)
			scalars.InsertTuple1(offset, s)

	grid.SetPoints(points)
	grid.GetPointData().SetVectors(vectors)
	# TODO: Einkommentieren für die Aufgabe mit farbigen Glyphs.
	#grid.GetPointData().SetScalars(scalars)
	return grid


def main():
	# TODO:
	# Erstellen sie eine Datenquelle fuer einen Pfeil.
	# Tipp: Wenn sie vtkArrowSource benutzen, muessen Sie den
	# Pfeil nicht selbst zusammenbauen.
	coneSource = vtk.vtkConeSource()
	sphereSource = vtk.vtkSphereSource()
	cubeSource = vtk.vtkCubeSource()
 
	# TODO: Verschieben Sie den Pfeil um -0.5 in x-Richtung.
	# Dazu benoetigen sie:
	# 	 - vtkTransform mit der genannten Verschiebung
	#   - vtkTransformPolyDataFilter bei dem die  
	#	   InputConnection auf den OutputPort der Datenquelle und
	#	   Transform auf die vorher in vtkTransform festgelegte Verschiebung
	#	   gesetzt werden

	
	# Tipp: Notfalls koennen Sie auch mit dem nicht verschobenen
	# Pfeil weiterarbeiten, dann ist es aber nicht so schoen.
 
	# TODO: Erstellen Sie nun ein Objekt vom Typ vtkGlyph3D.
	# - Setzen sie als SourceConnection den verschobenen Pfeil.
	# - Setzen sie als InputData die Rueckgabe des Methodenaufrufs 
	#   create_vectorfield(30,30).
	# - Setzen sie den VectorMode so, dass Vectoren benutzt werden
	#   (Bem.: Sonst werden evtl. die Normalen genommen).
	# - Setzen Sie die Skalierung auf 0.8.
	glyphSource = vtk.vtkGlyph3D()
	glyphSource.SetSourceConnection(0,coneSource.GetOutputPort())
	glyphSource.SetSourceConnection(1, sphereSource.GetOutputPort())
	glyphSource.SetSourceConnection(2, cubeSource.GetOutputPort())
	glyphSource.SetInputData(create_vectorfield(30,30))
	#Aufgabe 10c):
	#glyphSource.SetVectorModeToUseVector()
	glyphSource.SetIndexModeToVector()
	glyphSource.SetScaleFactor(0.7)
	# Aufgabe 10b):
	#glyphSource.OrientOff()

	#Aufgabe 10c):
	#glyphSource.SetRange(0,1)
	#glyphSource.SetScaleModeToDataScalingOff()
	
	# TODO: Erstellen Sie nun fuer das Glyhing-Objekt einen Mapper
	# und einen Actor.
	glyphMapper = vtk.vtkPolyDataMapper()
	glyphMapper.SetInputConnection(glyphSource.GetOutputPort())
	glyphActor = vtk.vtkActor()
	glyphActor.SetMapper(glyphMapper)


	renderer = vtk.vtkRenderer()
	renderWindow = vtk.vtkRenderWindow()
	renderWindow.AddRenderer(renderer)
	renderWindow.SetSize(640,640)

	renderer.AddActor(glyphActor)
	
	renderWindowInteractor = vtk.vtkRenderWindowInteractor()
	renderWindowInteractor.SetRenderWindow(renderWindow)
	
	renderWindow.Render()
	renderWindowInteractor.Start()

if __name__ == "__main__":
	main()
