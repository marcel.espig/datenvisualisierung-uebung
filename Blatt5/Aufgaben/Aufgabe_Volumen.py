# Übung zur Vorlesung "Datenvisualisierung"
# SS 2020
# Aufgabe_Volumen

import vtk
import math
import sys

# Es soll ein Volumendatensatz dargestellt werden.
# 
# Bearbeiten Sie die mit "TODO" gekennzeichneten Stellen.


def main():
	# Lesen des Volumendatensatzes
	v16 = vtk.vtkVolume16Reader()
	v16.SetDataDimensions(64, 64)
	v16.SetDataByteOrderToLittleEndian()
	# TODO:
	# Pruefen Sie den Pfad auf Korrektheit
	v16.SetFilePrefix("E:/DatenvisualisierungUebung/Projekt/datenvisualisierung-uebung/Blatt5/Data/headsq/quarter")
	v16.SetImageRange(1, 93)
	v16.SetDataSpacing(3.2, 3.2, 1.5)
	    
	# TODO: Erstellen sie eine Transfer-Funktion fuer 
	# die Transparenz. Verwenden Sie eine vtkPiecewiseFunction
	# und sorgen Sie dafuer, dass die Kurve durch die Punkte
	# (500,0); (1100,0.1) und (1150, 0.3) geht.
	transferFunction = vtk.vtkPiecewiseFunction()

	transferFunction.AddPoint(500, 0)
	transferFunction.AddPoint(1100, 0.1)
	transferFunction.AddPoint(1150, 0.3)
	#Aufgabe 23a):
	"""transferFunction.AddPoint(500,0)
	transferFunction.AddPoint(600, 0.1)
	transferFunction.AddPoint(700, 0.3)"""
	# Aufgabe 23b):
	"""transferFunction.AddPoint(200, 0)
	transferFunction.AddPoint(2500, 0.1)
	transferFunction.AddPoint(4000, 0.3)"""


	# TODO: Erstellen sie eine Transfer-Funktion fuer 
	# die Farbe. Verwenden Sie eine vtkColorTransferFunction
	# und sorgen Sie dafuer, dass die Kurve die folgende Farben
	# an die entsprechenden Werte vergibt:
	# 300: Rot 
	# 1200: Weiss
	color = vtk.vtkColorTransferFunction()
	color.AddRGBPoint(300, 255,0,0)
	color.AddRGBPoint(1200, 255,255,255)

	# TODO: Setzen Sie hier ihre Transferfunktionen ein:
	volumeProp = vtk.vtkVolumeProperty()
	volumeProp.SetColor(color)
	volumeProp.SetScalarOpacity(transferFunction)
	volumeProp.SetInterpolationTypeToLinear()
	volumeProp.ShadeOn()
	volumeProp.SetAmbient(0.2)
	volumeProp.SetDiffuse(1.0)

	# TODO: Erstellen Sie einen Mapper vom Typ
	# vtkFixedPointVolumeRayCastMapper und setzen sie den Input (dieser soll der Output des v16 Objektes sein).	
	mapper = vtk.vtkFixedPointVolumeRayCastMapper()
	mapper.SetInputConnection(v16.GetOutputPort())
	# TODO: Erzeugen Sie einen Actor vom Typ vtkVolume mit Namen "volume"
	# fuer die Darstellung.
	# Setzen Sie bei diesem den Mapper und die Volumeneigenschaften auf
	# die entsprechenden Objekte.
	volume = vtk.vtkVolume()
	volume.SetMapper(mapper)
	volume.SetProperty(volumeProp)

	renderer = vtk.vtkRenderer()
	renderWindow = vtk.vtkRenderWindow()
	renderWindow.AddRenderer(renderer)
	renderWindow.SetSize(640,640)

	# TODO:
	# Fügen Sie den Volumen-Actor dem Renderer hinzu:
	renderer.AddActor(volume)
	renderWindowInteractor = vtk.vtkRenderWindowInteractor()
	renderWindowInteractor.SetRenderWindow(renderWindow)
	
	renderWindow.Render()
	renderWindow.SetWindowName("Visualisierung von Volumendatensätzen")
	renderWindowInteractor.Start()

if __name__ == "__main__":
	main()

