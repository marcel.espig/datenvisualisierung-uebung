# Übung zur Vorlesung "Datenvisualisierung"
# SS 2020
# Aufgabe_Kontur

import vtk
import math
import sys

# Es soll eine ISO-Fläche für einen Volumendatensatz berechnet werden.
# 
# Bearbeiten Sie die mit "TODO" gekennzeichneten Stellen.


def main():
	# Lesen des Volumendatensatzes
	v16 = vtk.vtkVolume16Reader()
	v16.SetDataDimensions(64, 64)
	v16.SetDataByteOrderToLittleEndian()
	# TODO:
	# Pruefen Sie den Pfad auf Korrektheit
	v16.SetFilePrefix("E:/DatenvisualisierungUebung/Projekt/datenvisualisierung-uebung/Blatt5/Data/headsq/quarter")
	v16.SetImageRange(1, 93)
	v16.SetDataSpacing(3.2, 3.2, 1.5)
	# TODO:
	# Erstellen Sie eine Iso-Flaeche mit Hilfe eines vtkContourFilters.
	# Setzen Sie den Wert der einzigen zu berechnenden Iso-Flaeche mit 
	# Hilfe der Methode SetValue so, 
	# dass moeglichst genau die Haut aus den Volumendaten extrahiert wird.
	# Tipp: Materie, die wie Haut nur schwache Werte hervorruft, hat
	# einen Wert unter 1000, Knochen etwas darüber.
	contour = vtk.vtkContourFilter()
	contour.SetInputConnection(v16.GetOutputPort())
	contour.SetValue(0,400)
	# TODO:
	# Erstellen Sie einen Mapper und setzen Sie dessen Werteinterval
	# (ScalarRange) auf einen Ihrer Meinung nach geeigneten Wert.
	# Beachten Sie, dass es aufgrund der Standard-Farbtabelle
	# zu einem unnatuerlichen Farbton kommen wird.
	mapper = vtk.vtkPolyDataMapper()
	mapper.SetInputConnection(contour.GetOutputPort())
	mapper.SetScalarRange(0,1)
	# TODO:
	# Erstellen Sie einen Actor für den Mapper
	actor = vtk.vtkActor()
	actor.SetMapper(mapper)
	renderer = vtk.vtkRenderer()
	renderWindow = vtk.vtkRenderWindow()
	renderWindow.AddRenderer(renderer)
	renderWindow.SetSize(640,640)

	# TODO:
	# Fuegen Sie den Actor zum Renderer hinzu:
	renderer.AddActor(actor)
	renderWindowInteractor = vtk.vtkRenderWindowInteractor()
	renderWindowInteractor.SetRenderWindow(renderWindow)
	
	renderWindow.Render()
	renderWindow.SetWindowName("Visualisierung von ISO-Flaechen")
	renderWindowInteractor.Start()

if __name__ == "__main__":
	main()

