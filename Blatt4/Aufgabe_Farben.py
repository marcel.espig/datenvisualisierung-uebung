# Übung zur Vorlesung "Datenvisualisierung"
# SS 2020
# Aufgabe_Farben

import vtk
import math
import sys

# Es soll ein die Farbdarstellung einer Funktion bearbeitet werden.
# 
# Bearbeiten Sie die mit "TODO" gekennzeichneten Stellen.

part = 'a' # Auswahl der Teilaufgabe
v_min = 0 # enthält nach Aufruf von create_colormesh den Minimalwert der Farbfunktion
v_max = 0 # enthält nach Aufruf von create_colormesh den Maximalwert der Farbfunktion

# Hier wird das Netz erzeugt
def create_colormesh(dim1, dim2):
	global v_min
	global v_max
	mesh = vtk.vtkPolyData()
	polys = vtk.vtkCellArray()
	mesh.Allocate(dim1 * dim2, 0)

	# Einen Skalar für die Farbe
	scalars = vtk.vtkDoubleArray()
	scalars.SetNumberOfComponents(1)
	scalars.SetNumberOfTuples(dim1 * dim2)

	# Punkte für die Koordinaten
	points = vtk.vtkPoints()
	points.SetDataTypeToDouble()
	points.Allocate(0, dim1 * dim2)
	
	v_max = sys.float_info.min
	v_min = sys.float_info.max

	# Punkte
	for i in range(dim1):
		
		for j in range(dim2):
		
			x = i / 15.0
			y = j / 15.0
			z = (i + j) / 100.0
			val = math.sin(x) + math.cos(y) + math.sin(z)
			offset = i * dim2 + j
			points.InsertPoint(offset, x, y, val)
			scalars.InsertTuple1(offset, val)

			if val > v_max:
				v_max = val
			if val < v_min:
				v_min = val

	# Zellen
	for i in range(dim1 - 1):
		
		for j in range(dim2 - 1):
		
			polys.InsertNextCell(4) # Anzahl der Punkte
			polys.InsertCellPoint(i * dim2 + j)
			polys.InsertCellPoint(i * dim2 + j + 1)
			polys.InsertCellPoint(i * dim2 + j + dim2)
			polys.InsertCellPoint(i * dim2 + j + dim2 + 1)

	mesh.SetPoints(points)
	mesh.GetPointData().SetScalars(scalars)
	mesh.SetStrips(polys)
	return mesh



def main():
	# Das Netz als Datenquelle
	mesh = create_colormesh(314, 157)

	# TODO:
	# Erstellen sie eine Farbtabelle vom Typ vtkLookupTable
	if part == 'a':
		# Teil a (Parameterisierung der Farbtabelle)
		lookupTable = vtk.vtkLookupTable()
		lookupTable.SetHueRange(0.0,0.8)
		lookupTable.SetSaturationRange(1,1)
		lookupTable.SetValueRange(0.5,1)
		lookupTable.SetAlphaRange(0.5,1)
	else:
		# Teil b (Parameterisierung der Farbtabelle)
		lookupTable = vtk.vtkLookupTable()
		lookupTable.SetHueRange(0.0, 1)
		lookupTable.SetSaturationRange(0, 0)
		lookupTable.SetValueRange(0.2, 1)
		lookupTable.SetAlphaRange(0.3, 1)

	# Der Mapper für das Netz
	meshMapper = vtk.vtkPolyDataMapper()
	meshMapper.SetInputData(mesh)
	meshMapper.SetScalarRange(v_min, v_max)
	
	# TODO:
	# Binden Sie die Farbtabelle mit SetLookupTable in den Mapper ein.
	meshMapper.SetLookupTable(lookupTable)
	# Der Actor für das Netz
	meshActor = vtk.vtkActor()
	meshActor.SetMapper(meshMapper)

	# TODO:
	# Den Farbbalken erstellen und die Farbtabelle aus dem meshMapper setzen
	scalarBar=vtk.vtkScalarBarActor()
	scalarBar.SetLookupTable(meshMapper.GetLookupTable())
	renderer = vtk.vtkRenderer()
	renderWindow = vtk.vtkRenderWindow()
	renderWindow.AddRenderer(renderer)
	renderWindow.SetSize(640,640)

	# TODO:
	# die erstellten Aktoren dem Renderer hinzufügen
	renderer.AddActor(meshActor)
	renderer.AddActor(scalarBar)
	renderWindowInteractor = vtk.vtkRenderWindowInteractor()
	renderWindowInteractor.SetRenderWindow(renderWindow)
	
	renderWindow.Render()
	if part == 'a': # Teil a
		renderWindow.SetWindowName("Visualisierung von Skalaren eines Netzes durch Farben");
	else: # Teil b
		renderWindow.SetWindowName("Visualisierung von Skalaren eines Netzes durch Graustufen");
	renderWindowInteractor.Start()

if __name__ == "__main__":
	main()
