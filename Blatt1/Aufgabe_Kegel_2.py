# Übung zur Vorlesung "Datenvisualisierung"
# SS 2020
# Aufgabe_Kegel_2

import vtk

# Es soll ein Fenster dargestellt werden. Darin
# soll sich ein blauer Pfeil befinden, der aus einem Kegel
# und einem Zylinder zusammensetzt ist.
#
# Bearbeiten Sie die mit "TODO" gekennzeichneten Stellen.

def main():
	# Diesen Teil kenne Sie bereits aus 
	# Aufgabe_Kegel_1:
	coneSource = vtk.vtkConeSource()
	coneSource.SetHeight(3.0)
	coneSource.SetRadius(1.0)
	coneSource.SetResolution(10)

	coneMapper = vtk.vtkPolyDataMapper()
	coneMapper.SetInputConnection(coneSource.GetOutputPort())
	
	coneActor = vtk.vtkActor()
	coneActor.SetMapper(coneMapper)
	coneActor.GetProperty().SetColor(0.0, 0.0, 1.0);
	
	# TODO:
	# Erstellen Sie auf aehnliche Weise einen Zylinder
	# mit dem Radius 0.5, der Höhe 5 und einer Auflösung von 10.
	# Erstellen Sie dazu passend einen Mapper und einen Actor. 
	# Setzen Sie die Farbe auf Rot, Rotieren Sie den 
	# Zylinder um 90° um die Z-Achse und setzen Sie
	# seinen Position auf (-3.0, 0.0, 0.0).


	#Zylinder erstellen
	cylinderSource = vtk.vtkCylinderSource()
	cylinderSource.SetHeight(5)
	cylinderSource.SetRadius(0.5)
	cylinderSource.SetResolution(10)

	#Mapper und Actor erstellen
	cylinderMapper = vtk.vtkPolyDataMapper()
	cylinderMapper.SetInputConnection(cylinderSource.GetOutputPort())

	cylinderActor = vtk.vtkActor()
	cylinderActor.SetMapper(cylinderMapper)
	cylinderActor.GetProperty().SetColor(1.0, 0.0, 0.0)
	cylinderActor.SetPosition(-3.0, 0.0, 0.0)
	cylinderActor.RotateZ(90)


	# TODO: Erstellen Sie eine vtkAssembly und
	# fügen Sie den Kegel und den Zylinder hinzu
	assembly = vtk.vtkAssembly()
	assembly.AddPart(coneActor)
	assembly.AddPart(cylinderActor)
	# Renderer und Fenster erstellen
	renderer = vtk.vtkRenderer()
	renderWindow = vtk.vtkRenderWindow()
	renderWindow.AddRenderer(renderer)
	renderWindow.SetSize(640,480)

	# TODO: Fügen Sie die für den Pfeil erstellte vtkAssembly dem Renderer als Actor hinzu
	renderer.AddActor(assembly)
	# Interactor erstellen
	renderWindowInteractor = vtk.vtkRenderWindowInteractor()
	renderWindowInteractor.SetRenderWindow(renderWindow)
		
	# Schliesslich das Fenster anzeigen:
	renderWindow.Render()
	renderWindowInteractor.Start()

if __name__ == "__main__":
	main()