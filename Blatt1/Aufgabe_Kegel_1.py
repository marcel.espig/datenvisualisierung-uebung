# Übung zur Vorlesung "Datenvisualisierung"
# SS 2020
# Aufgabe_Kegel_1

import vtk

# Es soll ein Fenster dargestellt werden. Darin
# soll sich ein blauer Kegel auf grünem Hintergrund befinden.
#
# Bearbeiten Sie die mit "TODO" gekennzeichneten Stellen.

def main():
	# Ab hier bauen Sie bitte die Imaging-Pipeline auf.

	# TODO: Erstellen sie hier einen Kegel (vtkConeSource).
	# Setzen Sie die Hoehe auf 3, den Radius auf 1 und
	# die Kantenzahl (Resolution) auf 10.
	coneSource = vtk.vtkConeSource()
	coneSource.SetHeight(3.0)
	coneSource.SetRadius(1.0)
	coneSource.SetResolution(10)

	# TODO: Erstellen Sie nun eine Mapper (vtkPolyDataMapper)
	# und setzten Sie dessen InputConnection auf den OutputPort
	# des Kegels.
	coneMapper = vtk.vtkPolyDataMapper()
	coneMapper.SetInputConnection(coneSource.GetOutputPort())

	# TODO: Erstellen sie einen Actor (vtkActor).
	# Setzen Sie den Mapper den Sie gerade erstellt haben
	# als den Mapper diese Actors ein. Ermitteln sie mittels
	# GetProperty() die Eigenschaften des Actors und setzen Sie
	# darin die Farbe auf Blau.
	coneActor = vtk.vtkActor()
	coneActor.SetMapper(coneMapper)
	coneActor.GetProperty().SetColor(0.0, 0.0, 1.0)

	# Erstellen eines Renderers (vtkRenderer) und eines Fensters (vtkRenderWindow).
	# sowie hizufügen des Renderers zu dem Fenster
	renderer = vtk.vtkRenderer()
	renderWindow = vtk.vtkRenderWindow()

	renderWindow.AddRenderer(renderer)

	# TODO: setzen Sie die Fenstergröße auf 640x480 Pixel
	renderWindow.SetSize(640, 480)
	# Erstellen eine RenderWindowInteractor (vtkRenderWindowInteractor)
	# und setzen des RenderWindow auf das gerade erstellte Fenster
	renderWindowInteractor = vtk.vtkRenderWindowInteractor()
	renderWindowInteractor.SetRenderWindow(renderWindow)

	# TODO: Fügen Sie den für den Kegel erstellten Actor dem Renderer hinzu
	renderer.AddActor(coneActor)

	# Schliesslich das Fenster anzeigen:
	renderWindow.Render()
	renderWindowInteractor.Start()

if __name__ == "__main__":
	main()
