import vtk

def main():
    scale = [10.0,1.0,1.0]
    #Erster Pfeil (X-Achse):
    arrowSource=vtk.vtkArrowSource()
    arrowSource.SetShaftRadius(0.5)
    arrowSource.SetShaftResolution(10)
    arrowSource.SetTipResolution(10)
    arrowSource.SetTipLength(3.0/10.0)
    arrowSource.SetTipRadius(1.0)
    arrowMapper = vtk.vtkPolyDataMapper()
    arrowMapper.SetInputConnection(arrowSource.GetOutputPort())

    arrowActor = vtk.vtkActor()
    arrowActor.SetMapper(arrowMapper)
    arrowActor.GetProperty().SetColor(0.0, 0.0, 1.0)
    arrowActor.SetPosition(-3.0, 0.0, 0.0)
    arrowActor.SetScale(scale)
    #Zweiter Pfeil(Y-Achse):
    arrowSource2 = vtk.vtkArrowSource()
    arrowSource2.SetShaftRadius(0.5)
    arrowSource2.SetShaftResolution(10)
    arrowSource2.SetTipResolution(10)
    arrowSource2.SetTipLength(3.0/10.0)
    arrowSource2.SetTipRadius(1.0)
    arrowMapper2 = vtk.vtkPolyDataMapper()
    arrowMapper2.SetInputConnection(arrowSource2.GetOutputPort())

    arrowActor2 = vtk.vtkActor()
    arrowActor2.SetMapper(arrowMapper2)
    arrowActor2.GetProperty().SetColor(0.0, 0.0, 1.0)
    arrowActor2.SetPosition(-3.0, 0.0, 0.0)
    arrowActor2.RotateZ(90)
    arrowActor2.SetScale(scale)

    #Dritter Pfeil(Z-Achse):
    arrowSource3 = vtk.vtkArrowSource()
    arrowSource3.SetShaftRadius(0.5)
    arrowSource3.SetShaftResolution(10)
    arrowSource3.SetTipResolution(10)
    arrowSource3.SetTipLength(3.0/10.0)
    arrowSource3.SetTipRadius(1.0)
    arrowMapper3 = vtk.vtkPolyDataMapper()
    arrowMapper3.SetInputConnection(arrowSource3.GetOutputPort())

    arrowActor3 = vtk.vtkActor()
    arrowActor3.SetMapper(arrowMapper3)
    arrowActor3.GetProperty().SetColor(0.0, 0.0, 1.0)
    arrowActor3.SetPosition(-3.0, 0.0, 0.0)
    arrowActor3.RotateY(90)
    arrowActor3.SetScale(scale)

    assembly = vtk.vtkAssembly()
    assembly.AddPart(arrowActor)
    assembly.AddPart(arrowActor2)
    assembly.AddPart(arrowActor3)





    renderer = vtk.vtkRenderer()
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)
    renderWindow.SetSize(640, 480)

    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)
    renderer.AddActor(assembly)

    renderWindow.Render()
    renderWindowInteractor.Start()









if __name__ == "__main__":
    main()